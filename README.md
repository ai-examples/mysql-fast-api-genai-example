# MySQL Fast API GenAI Example
This is a demo repo that contains MySQL and a Web API front end to act as bridge for GenAI queries.

## Getting started
### Docker Compose
```
docker compose up
```

### Testing
```
docker compose up db
python -m pytest tests/
```