from flask import Flask, jsonify
import os
import mysql.connector

app = Flask(__name__)

def get_db_connection():
    conn = mysql.connector.connect(
        host=os.getenv('HOST'),
        database=os.getenv('MYSQL_DATABASE'),
        user=os.getenv('MYSQL_ROOT_USERNAME'),
        password=os.getenv('MYSQL_ROOT_PASSWORD'))
    return conn

def dict_cursor(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@app.route("/")
def test_print():
    return "This is the landing page."

@app.route("/users")
def read_users():
    conn = get_db_connection()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM users;")
    data = dict_cursor(cursor)
    cursor.close()
    conn.close()
    return jsonify(data)

@app.route("/battery_data")
def read_battery_data():
    conn = get_db_connection()
    cursor = conn.cursor()
    cursor.execute("SELECT * FROM battery_data;")
    data = dict_cursor(cursor)
    cursor.close()
    conn.close()
    return jsonify(data)

if __name__ == "__main__":
    app.run()