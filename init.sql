-- Create a users sample table
CREATE TABLE users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    email VARCHAR(100)
);

-- Create a new table for battery data
CREATE TABLE battery_data (
    id INT AUTO_INCREMENT PRIMARY KEY,
    timestamp DATETIME(3),
    supply_voltage FLOAT,
    supply_current FLOAT,
    load_voltage FLOAT,
    load_current FLOAT,
    temperature FLOAT,
    voltage INT,
    current INT,
    relative_state_of_charge INT,
    absolute_state_of_charge INT,
    remaining_capacity INT,
    run_time_to_empty INT
);

-- Insert some sample data
INSERT INTO users (name, email) VALUES ('Alice', 'alice@example.com');
INSERT INTO users (name, email) VALUES ('Bob', 'bob@example.com');
