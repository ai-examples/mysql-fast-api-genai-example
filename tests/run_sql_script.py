import mysql.connector
import argparse

def run_sql_script(file_path, host, user, password, database):
    """
    Executes an SQL script from a specified file on a MySQL database.

    The function reads an SQL script from a file and executes the commands 
    contained within the script on the specified MySQL database. It splits 
    the script into individual SQL statements and executes them sequentially. 
    If any of the commands fail, the function will rollback any changes 
    made to the database before the error occurred.

    Arguments:
    file_path : str
        The path to the SQL file that contains the SQL script to be executed.
    host : str
        The hostname or IP address of the MySQL server.
    user : str
        The username used to authenticate with the MySQL server.
    password : str
        The password used to authenticate with the MySQL server.
    database : str
        The name of the database where the script will be executed.

    Returns:
    None
        The function does not return a value. It prints a success message 
        upon successful execution of the script, or an error message if 
        an exception occurs.
    """
    # Read SQL file
    with open(file_path, 'r') as file:
        sql_script = file.read()

    # Connect to the database
    connection = mysql.connector.connect(
        host=host,
        user=user,
        password=password,
        database=database
    )

    # Create a cursor object
    cursor = connection.cursor()

    # Execute the SQL script
    exception_occurred = None
    try:
        for command in sql_script.split(';'):
            if command.strip() != '':
                cursor.execute(command)
        connection.commit()
    except Exception as e:
        print("An error occurred, rolling back changes...")
        connection.rollback()
        exception_occurred = e
    finally:
        # Close cursor and connection
        cursor.close()
        connection.close()

        # Re-raise the exception if one occurred
        if exception_occurred is not None:
            raise exception_occurred

if __name__ == "__main__":
    # Create the parser
    parser = argparse.ArgumentParser(description="Run an SQL script on a MySQL database.")

    # Add arguments
    parser.add_argument("filepath", help="The path to the SQL file to be executed.")
    parser.add_argument("host", help="Hostname or IP address of the MySQL server.")
    parser.add_argument("user", help="Username for the MySQL server.")
    parser.add_argument("password", help="Password for the MySQL server.")
    parser.add_argument("database", help="Name of the database.")

    # Parse arguments
    args = parser.parse_args()

    # Run the SQL script
    run_sql_script(args.filepath, args.host, args.user, args.password, args.database)