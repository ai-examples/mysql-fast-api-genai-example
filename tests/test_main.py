from flask.testing import FlaskClient
from main import app  # Import your Flask app
import csv
import mysql.connector
import os
import pytest
import sys
import yaml

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Fixture to set environment variables for tests
@pytest.fixture(scope="session", autouse=True)
def set_env_vars():
    # Read the docker-compose.yml file
    with open('docker-compose.yml', 'r') as file:
        compose_content = yaml.safe_load(file)
        env_vars = compose_content['x-environment']

    # Set environment variables
    old_env_vars = {}
    for key, value in env_vars.items():
        old_env_vars[key] = os.getenv(key)
        os.environ[key] = value

    # Exception: Set HOST to 'localhost' or 'mysql' based on CI environment
    os.environ['HOST'] = 'localhost'

    # Yield to allow tests to run
    yield

    # Teardown (unset environment variables)
    for key in env_vars:
        if old_env_vars[key] is not None:
            os.environ[key] = old_env_vars[key]
        else:
            del os.environ[key]

@pytest.fixture(scope="module")
def test_app():
    # Create a Flask test client using the Flask app
    app.config['TESTING'] = True
    client = app.test_client()
    yield client  # Testing happens here

@pytest.fixture(scope="module")
def db_connection():
    conn = mysql.connector.connect(
        host=os.getenv('HOST'),
        database=os.getenv('MYSQL_DATABASE'),
        user=os.getenv('MYSQL_ROOT_USERNAME'),
        password=os.getenv('MYSQL_ROOT_PASSWORD'))
    cursor = conn.cursor()

    if os.getenv('CI') == 'true':
        print("Configuring DB for CI tests")

    yield cursor, conn

    # Teardown code: close cursor and database connection
    cursor.close()
    conn.close()

class TestDockerCompose:
    # Example test using the environment variable
    def test_example(self):
        assert os.getenv('MYSQL_ROOT_PASSWORD') == 'password'
        # Add more test logic here

class TestAPI:
    @staticmethod
    def load_csv_to_database(db_connection, csv_file_path):
        cursor, conn = db_connection

        # Read the CSV file
        with open(csv_file_path, newline='') as csvfile:
            reader = csv.reader(csvfile)
            next(reader, None)  # Skip the header row

            # Clear existing data in the table
            conn.cursor("DELETE FROM battery_data;")

            # Insert new data from the CSV
            for row in reader:
                conn.cursor(
                    "INSERT INTO battery_data (timestamp, supply_voltage, supply_current, load_voltage, load_current, temperature, voltage, current, relative_state_of_charge, absolute_state_of_charge, remaining_capacity, run_time_to_empty) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);",
                    row
                )
            conn.commit()

    def test_landing_page(self, test_app):
        # Issue a GET request to the / endpoint
        response = test_app.get("/")

        # Validate the response
        assert response.status_code == 200
        assert response.data.decode('utf-8') == 'This is the landing page.'

    def test_read_users(self, test_app, db_connection):
        cursor, conn = db_connection

        # Execute test against the database
        cursor.execute("SELECT * FROM users;")
        users = cursor.fetchall()

        # Issue a GET request to the /users endpoint
        response = test_app.get("/users")
        
        # Validate the response
        assert response.status_code == 200
        response_data = response.get_json()
        assert isinstance(response_data, list)  # Assuming the response is a list

        # Validate that the response matches the data in the database
        for user, expected_user in zip(response_data, users):
            assert user['id'] == expected_user[0]
            assert user['name'] == expected_user[1]
            assert user['email'] == expected_user[2]

    def test_read_battery_data(self, test_app, db_connection):
        cursor, conn = db_connection

        # Load the CSV data into the database
        self.load_csv_to_database(db_connection, "minimal_battery_test_log.csv")

        # Execute test against the database
        cursor.execute("SELECT * FROM battery_data;")
        battery_data_records = cursor.fetchall()

        # Issue a GET request to the /battery_data endpoint
        response = test_app.get("/battery_data")

        # Validate the response
        assert response.status_code == 200
        response_data = response.get_json()
        assert isinstance(response_data, list)  # Assuming the response is a list

        # Validate that the response matches the data in the database
        for record, expected_record in zip(response_data, battery_data_records):
            assert record['id'] == expected_record[0]
            assert record['timestamp'] == expected_record[1].strftime('%Y-%m-%d %H:%M:%S.%f')
            assert record['supply_voltage'] == expected_record[2]
            assert record['supply_current'] == expected_record[3]
            assert record['load_voltage'] == expected_record[4]
            assert record['load_current'] == expected_record[5]
            assert record['temperature'] == expected_record[6]
            assert record['voltage'] == expected_record[7]
            assert record['current'] == expected_record[8]
            assert record['relative_state_of_charge'] == expected_record[9]
            assert record['absolute_state_of_charge'] == expected_record[10]
            assert record['remaining_capacity'] == expected_record[11]
            assert record['run_time_to_empty'] == expected_record[12]